# Purpose:
#   Read CALIPSO data
#
# by Hong Chen (me@hongchen.cz)
#
# Tested on macOS v10.12.6 with
#   - Python v3.6.0

import os
import sys
import glob
import datetime
import numpy as np
import matplotlib.path as mpl_path
import cartopy.crs as ccrs
import ftplib
from pyhdf.SD import SD, SDC
from scipy import interpolate
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.ticker import FixedLocator
from matplotlib import rcParams
import matplotlib.patches as patches
import cartopy.crs as ccrs





def GTIME(fname, jday=True):

    filename = os.path.basename(fname)
    words    = filename.split('.')
    # e.g. 2016-09-20T11-30-00
    dtime    = datetime.datetime.strptime(words[-2][:-2], '%Y-%m-%dT%H-%M-%S')
    if jday:
        return (dtime-datetime.datetime(dtime.year-1, 12, 31)).total_seconds() / 86400.0
    else:
        return dtime






class CALIPSO_AER_LAYER_L2:

    """
    input:
        namePattern: e.g. CAL_LID_L2_05kmALay-Standard-V4-10.2017-08-13T13-11-26ZD.hdf
        vnameExtra: default is '', can be "532" etc.
        fdir: the data directory

    output:
        a class object that contains:
            1. self.lon
            2. self.lat
            3. self.ctp: cloud thermodynamic phase
            4. self.cot
            5. self.cer
            6. self.cot_pcl
            7. self.cer_pcl
            6. self.cot_unc
            7. self.cer_unc
            8. self.COLLOCATE(lon_in, lat_in):
                8.1. self.lon_domain
                8.2. self.lat_domain
                8.3. self.cot_domain
                8.4. self.cer_domain
                8.5. self.cot_pcl_domain
                8.6. self.cer_pcl_domain
                8.5. self.cot_unc_domain
                8.6. self.cer_unc_domain
                8.7. self.ctp_domain
                8.8 . self.lon_collo
                8.9 . self.lat_collo
                8.10. self.cot_collo
                8.11. self.cer_collo
                8.12. self.cot_pcl_collo
                8.13. self.cer_pcl_collo
                8.12. self.cot_unc_collo
                8.13. self.cer_unc_collo
                8.14. self.cot_collo_all
                8.15. self.cer_collo_all
                8.16. self.ctp_collo
    """

    def __init__(self, fname, aodTag='532'):

        fname  = os.path.abspath(fname)
        if not os.path.isfile(fname):
            exit('Error [CALIPSO_AER_LAYER_L2]: can not find file \'%s\'.' % (fname))

        self.fname = fname

        fname_aer = fname
        f_aer = SD(fname_aer, SDC.READ)

        # vnames = f_aer.datasets().keys()
        # for vname in vnames:
        #     print('+')
        #     print(vname)
        #     for att in f_aer.select(vname).attributes().keys():
        #         print('    %s: %s' % (att, f_aer.select(vname).attributes()[att]))
        #     print('-')
        # exit()

        lon = f_aer.select('Longitude')[:]
        lon[lon<0.0] += 360.0
        self.lon = np.mean(lon, axis=1)
        self.lat = np.mean(f_aer.select('Latitude')[:], axis=1)

        vname          = 'Column_Optical_Depth_Tropospheric_Aerosols_%s' % (aodTag)
        self.aod_t     = np.squeeze(f_aer.select(vname)[:])
        vname          = 'Column_Optical_Depth_Tropospheric_Aerosols_Uncertainty_%s' % (aodTag)
        self.aod_t_unc = np.squeeze(f_aer.select(vname)[:])

        vname          = 'Column_Optical_Depth_Stratospheric_Aerosols_%s' % (aodTag)
        self.aod_s     = np.squeeze(f_aer.select(vname)[:])
        vname          = 'Column_Optical_Depth_Stratospheric_Aerosols_Uncertainty_%s' % (aodTag)
        self.aod_s_unc = np.squeeze(f_aer.select(vname)[:])

        f_aer.end()



if __name__ == '__main__':

    fname = 'data/CAL_LID_L2_05kmALay-Standard-V4-10.2017-08-13T13-11-26ZD.hdf'
    sat = CALIPSO_AER_LAYER_L2(fname)
    # EARTH_VIEW(sat)

    # dtime = GTIME(fname, jday=False)
    # print(dtime)
